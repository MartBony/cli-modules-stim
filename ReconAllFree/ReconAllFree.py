#!/usr/bin/env python-real

import sys
import subprocess
import time
import os
from ressources.outputExpectedFiles import files
import shutil
import shlex

moduleDir = os.path.dirname(__file__)

dockerDowloadPercentage = 1/100


# See https://nipype.readthedocs.io/en/latest/api/generated/nipype.interfaces.freesurfer.preprocess.html#reconall

def reportProgress(percentage):
    print(percentage)
    print(f"<filter-progress>{percentage}</filter-progress>")
    sys.stdout.flush()

def countFilesInDir(dir):
    count = 0
    with os.scandir(dir) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                count+=1
    return count



def monitorFSProgress(subject_output_dir, process):

    total_progress = sum(len(listContent) for listContent in files.values())


    # Prevent progress dialog from automatically closing
    finishedFiles = []
    exitCode = 1

    while(True):
        for folder in files:
            for fileName in files[folder]:
                
                if(fileName not in finishedFiles and os.path.exists(os.path.join(subject_output_dir, folder, fileName))):
                    finishedFiles.append(fileName)

        reportProgress(dockerDowloadPercentage + (len(finishedFiles)/(total_progress+1)*(1-dockerDowloadPercentage)))
        try:
            exitCode = process.wait(20)
            break
        except subprocess.TimeoutExpired:
            continue

    reportProgress(1)
    print("Process ended")
    return exitCode
    







def main(t1ImagePath, outputDir, subjectName, fsLicensePath):


    customFSDockerImage = "licensedfreesurfer_public"

    # Check if image already exists
    try:
        checkOut = subprocess.check_output(
            ["docker", "image", "inspect", customFSDockerImage],
            text=True,
            encoding="utf-8"
            ) # Will fail if image deosn't exist
        

    except Exception as exc:
        # Build image with fs license based on freesurfer

        freeSurfDockerImage = "dmalt/freesurfer_public:7.1.1"
        localLicensePath = os.path.join(moduleDir, "license.txt")
        shutil.copy(fsLicensePath, localLicensePath)
        with open(os.path.join(moduleDir, "Dockerfile"), 'w') as dockerfile:
            dockerfile.write(f"FROM {freeSurfDockerImage}")
            dockerfile.write("\n")
            dockerfile.write(f"COPY license.txt /usr/local/freesurfer/.license")

        subprocess.run(["docker", "build", "-t", customFSDockerImage, moduleDir])


    reportProgress(dockerDowloadPercentage)

    # Run Recon All
    docker_command = [
        "docker", "run",
        "--rm",
        "-e", f"LOCAL_USER_ID={os.getuid()}",
        "-v", f"{os.path.dirname(t1ImagePath)}:/data",
        "-v", f"{outputDir}:/output",
        customFSDockerImage,
        "fsf_recon_all",
        f"subject={subjectName}",
        f"recon_all_cmd='-i /data/{os.path.basename(t1ImagePath)} -sd /output -all -parallel -openmp 8'"
    ]

    # Here we have to run the command in shell otherwise compatibility problems
    shell_formatted_d_command = " ".join(docker_command)

    print(shell_formatted_d_command)
    
    return subprocess.Popen(
        shell_formatted_d_command, 
        shell=True
        )
    





if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ReconAllFree <t1ImagePath> <outputDir> <subjectName> <fsLicensePath>")
        sys.exit(1)

    reportProgress(0)
    process = main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    subject_output_dir = os.path.join(sys.argv[2], sys.argv[3])
    exitCode = monitorFSProgress(subject_output_dir, process)
    time.sleep(1)
    sys.exit(exitCode)