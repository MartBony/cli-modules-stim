#!/usr/bin/env python-real

import sys
import subprocess

import shlex

print("""<filter-start><filter-name>TestFilter</filter-name><filter-comment>ibid</filter-comment></filter-start>""")
sys.stdout.flush()

def main(command, shell):
    if(not shell):
        command = shlex.split(command)

    process = subprocess.Popen(
        command, 
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell = bool(shell),
        text = True,
        encoding="utf-8"
        )


    for line in process.stdout:
        print(line, end="")
    
    
    #Uneccesary but just in case
    stdout, stderr = process.communicate()


    return (process.poll(), stderr)




if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: BackgroundProcess <command> (<shell>)")
        sys.exit(1)

    shell = sys.argv if len(sys.argv) >= 3 else False

    exitData = main(sys.argv[1], shell)
    
    print("""<filter-end><filter-name>TestFilter</filter-name><filter-time>10</filter-time></filter-end>""")
    sys.stdout.flush()

    if(exitData[0]):
        sys.exit(exitData[1])
    sys.exit(exitData[0])