#!/usr/bin/env python-real

import sys
import subprocess
import time
import os
from ressources.outputExpectedFiles import files

# See https://nipype.readthedocs.io/en/latest/api/generated/nipype.interfaces.freesurfer.preprocess.html#reconall

dockerDowloadPercentage = 1/100

def reportProgress(percentage):
    print(percentage)
    print(f"<filter-progress>{percentage}</filter-progress>")
    sys.stdout.flush()

def countFilesInDir(dir):
    count = 0
    with os.scandir(dir) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                count+=1
    return count




def monitorFSProgress(subject_output_dir, process):

    total_progress = sum(len(listContent) for listContent in files.values())


    # Prevent progress dialog from automatically closing
    finishedFiles = []
    exitCode = 1

    while(True):
        for folder in files:
            for fileName in files[folder]:
                
                if(fileName not in finishedFiles and os.path.exists(os.path.join(subject_output_dir, folder, fileName))):
                    finishedFiles.append(fileName)

        reportProgress(dockerDowloadPercentage + (len(finishedFiles)/(total_progress+1)*(1-dockerDowloadPercentage)))

        try:
            exitCode = process.wait(20)
            break
        except subprocess.TimeoutExpired:
            continue

    reportProgress(1)
    print("Process ended")

    return exitCode
    







def main(t1ImagePath, outputDir, subjectName, fsLicensePath):
    fastSurfDockerImage = "deepmi/fastsurfer:latest"
   

    # Check if image already exists
    try:
        checkOut = subprocess.check_output(
            ["docker", "image", "inspect", fastSurfDockerImage],
            text=True,
            encoding="utf-8"
            ) # Will fail if image deosn't exist
        
    except Exception as exc:
        subprocess.run(["docker", "pull", fastSurfDockerImage])


    reportProgress(dockerDowloadPercentage)

    docker_command = [
        "docker", "run",
        "--gpus", "all",
        "--rm",
        "--user", f"{os.getuid()}:{os.getgid()}",
        "-v", f"{os.path.dirname(t1ImagePath)}:/data",
        "-v", f"{outputDir}:/output",
        "-v", f"{os.path.dirname(fsLicensePath)}:/fs_license",
        fastSurfDockerImage,
        "--t1", f"/data/{os.path.basename(t1ImagePath)}",
        "--sd", "/output",
        "--sid", subjectName,
        "--fs_license", f"/fs_license/{os.path.basename(fsLicensePath)}",
        "--parallel", 
        "--threads", "10",
        "--fsaparc",
        #"--seg_only"
    ]

    print(*docker_command)

        
    return subprocess.Popen(docker_command)
    


if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ReconAllFast <t1ImagePath> <outputDir> <subjectName> <fsLicensePath>")
        sys.exit(1)

    reportProgress(0)
    process = main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]) # non blocking
    subject_output_dir = os.path.join(sys.argv[2], sys.argv[3])
    exitCode = monitorFSProgress(subject_output_dir, process) # blocking
    time.sleep(1) # To leave ime for progress to update before the cli module ends
    sys.exit(exitCode)